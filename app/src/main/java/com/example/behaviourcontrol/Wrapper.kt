package com.example.behaviourcontrol

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.KeyEvent
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity

/*class CustomWebViewClient : WebViewClient() {
    override fun onReceivedError(webview: WebView, errorCode: Int, description: String, failingUrl: String) {

*//*        val htmlData = "error";

        webview.loadUrl("about:blank");
        webview.loadDataWithBaseURL(null, htmlData, "text/html", "UTF-8", null);
        webview.invalidate();*//*
        val context = Wrapper()
        val intent = Intent(context, Error::class.java)
        intent.putExtra("errorCode", errorCode)
        startActivity(intent)

    }
}*/

class Wrapper : AppCompatActivity() {
    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        val bdWebview = findViewById<WebView>(R.id.bd_webview)
        if (keyCode == KeyEvent.KEYCODE_BACK && bdWebview.canGoBack()) {
            bdWebview.goBack()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wrapper)
        val bdWebview = findViewById<WebView>(R.id.bd_webview)
        var url = intent.getStringExtra("url")
        val language = intent.getStringExtra("language")

        bdWebview.webViewClient = WebViewClient()
        bdWebview.settings.javaScriptEnabled = true
        bdWebview.settings.domStorageEnabled = true
        if (url != null) {
            if (!url.startsWith("http://")) {
                url = "http://${url}"
            }
            bdWebview.loadUrl("${url}/${language}")
        }
    }
}