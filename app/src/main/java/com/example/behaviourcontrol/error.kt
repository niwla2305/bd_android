package com.example.behaviourcontrol

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class error : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_error)
        val errorCode = intent.getIntExtra("errorCode", 0)
        val errorDescriptionView = findViewById<TextView>(R.id.errorDescription)
        errorDescriptionView.text = errorCode.toString()
    }
}