package com.example.behaviourcontrol

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import androidx.core.os.ConfigurationCompat

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val locale = ConfigurationCompat.getLocales(Resources.getSystem().configuration).get(0)

        val prefs = getSharedPreferences(packageName + "_preferences", Context.MODE_PRIVATE)
        val editor = prefs.edit()

        val connectBtn = findViewById<Button>(R.id.connectBtn)
        val editURL = findViewById<EditText>(R.id.editURL)
        val languageSelect = findViewById<RadioGroup>(R.id.languageSelect)

        val languageEN = findViewById<RadioButton>(R.id.langEN)
        val languageDE = findViewById<RadioButton>(R.id.lanDE)

        when (locale.language) {
            "de" -> languageSelect.check(languageDE.id)
            "en" -> languageSelect.check(languageEN.id)
            else -> { // Note the block
                languageSelect.check(languageEN.id)
            }
        }


        editURL.setText(prefs.getString("url", ""))

        connectBtn.setOnClickListener {
            val message = editURL.text.toString()

            val selectedLangId: Int = languageSelect.checkedRadioButtonId
            val language: RadioButton = findViewById(selectedLangId)

            editor.putString("url", message)
            editor.apply()

            val intent = Intent(this, Wrapper::class.java)
            intent.putExtra("url", message)
            intent.putExtra("language", language.tag.toString())
            startActivity(intent)
        }
    }
}